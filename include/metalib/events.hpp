//
// Created by Sam Fuller on 9/13/15.
//

#ifndef METALANCER_FUNCTIONLIST_HPP
#define METALANCER_FUNCTIONLIST_HPP

#include <functional>
#include <unordered_set>

namespace metalancer {


    template <typename ... Args> class Subscription;

    template <typename ... Args>
    class Event {

        std::unordered_set<const Subscription<Args...>*> subscriptors;

    public:

        Event& operator +=(const Subscription<Args...>& subscription) {
            subscriptors.insert(&subscription);
            return *this;
        }

        Event& operator -=(const Subscription<Args...>& subscription) {
            subscriptors.erase(&subscription);
            return *this;
        }

        void invoke(Args... args) {
            // TODO: Locking of this event
            for(auto subscriptor : subscriptors) {
                subscriptor->func(args...);
            }
        }

    };

    template <typename  ... Args>
    class Subscription {

        std::weak_ptr<Event<Args...>> _event;

    public:

        std::function<void(Args...)> func;

        Subscription() {}
        Subscription(const std::function<void(Args...)>& func) :
            func(func)
        {}

        Subscription(const Subscription& other) {
            func = other.func;
            if(auto event = other._event.lock()) {
                set_event(event);
            }
        }

        Subscription& operator=(const Subscription& other) {
            func = other.func;
            if(auto event = other._event.lock()) {
                set_event(event);
            }
        }

        Subscription(Subscription&& other) {
            std::swap(_event, other._event);
            func = other.func;
        }

        Subscription& operator=(Subscription&& other) {
            std::swap(_event, other._event);
            func = other.func;
        }

        ~Subscription() {
            if(auto event = _event.lock()) {
                event->operator-=(*this);
            }
        }

        void set_event(const std::shared_ptr<Event<Args...>>& event) {
            if(auto previous_event = _event.lock()) {
                previous_event->operator-=(*this);
            }
            _event = event;
            event->operator+=(*this);
        }

    };





}


#endif //METALANCER_FUNCTIONLIST_HPP
