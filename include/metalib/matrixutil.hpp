//
// Created by Sam Fuller on 10/3/15.
//

#ifndef BIZOMATIC_MATRIXUTIL_HPP
#define BIZOMATIC_MATRIXUTIL_HPP

#include "../../../metaengine/include/mane/Matrix4.hpp"

namespace metalancer {
    mane::Matrix4 make_perspective(float fovy, float aspect, float z_near, float z_far);
}

#endif //BIZOMATIC_MATRIXUTIL_HPP
