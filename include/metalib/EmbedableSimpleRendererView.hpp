//
// Created by Sam Fuller on 11/3/15.
//

#ifndef LOSTLIGHT_EMBEDABLESIMPLERENDERERVIEW_HPP
#define LOSTLIGHT_EMBEDABLESIMPLERENDERERVIEW_HPP

#include "SimpleRendererView.hpp"

namespace metalancer {
    class EmbedableSimpleRendererView : public SimpleRendererView {

    public:

        EmbedableSimpleRendererView(DisplayList& dl);

        DisplayNode& node();

    };
}


#endif //LOSTLIGHT_EMBEDABLESIMPLERENDERERVIEW_HPP
