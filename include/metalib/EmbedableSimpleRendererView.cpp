//
// Created by Sam Fuller on 11/3/15.
//

#include "EmbedableSimpleRendererView.hpp"

using namespace metalancer;

EmbedableSimpleRendererView::EmbedableSimpleRendererView(DisplayList &dl) :
    SimpleRendererView(dl)
{ }

DisplayNode& EmbedableSimpleRendererView::node() { return SimpleRendererView::node; }
