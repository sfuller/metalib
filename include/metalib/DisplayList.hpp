//
// Created by sfuller on 8/9/15.
//

#ifndef METALANCER_DISPLAYLIST_HPP
#define METALANCER_DISPLAYLIST_HPP

#include <vector>
#include <mane/Engine.hpp>
#include <mane/Camera.hpp>
#include <mane/MeshRenderer.hpp>
#include <memory>

namespace metalancer {

    class DisplayNodeController {
    public:
        mane::MeshRenderer* renderer;
        bool active;

        DisplayNodeController();
        virtual ~DisplayNodeController();
    };

    class LinkedDisplayListController : public DisplayNodeController {
    public:
        LinkedDisplayListController* prev;
        LinkedDisplayListController* next;

        LinkedDisplayListController(LinkedDisplayListController* prev);
        virtual ~LinkedDisplayListController();
    };

    class DisplayList {

        mane::Engine* _engine;
        const mane::Camera* _camera;
        LinkedDisplayListController _start;
        LinkedDisplayListController _end;

    public:
        DisplayList(mane::Engine* engine, const mane::Camera* camera);

        DisplayNodeController* add();
        void render() const;
        mane::Engine* engine() const;
        const mane::Camera* camera() const;
    };

    class DisplayNode {
        DisplayNodeController* _controller;
        mane::MeshRenderer _renderer;
    public:
        DisplayNode(mane::Engine* engine);
        DisplayNode(DisplayList& dl);
        DisplayNode(const DisplayNode& other) = delete;
        DisplayNode(DisplayNode&& node);
        DisplayNode& operator=(const DisplayNode& other) = delete;
        DisplayNode& operator=(DisplayNode&& other);
        ~DisplayNode();
        mane::MeshRenderer& renderer();
        void set_active(bool active);

    };

//    class DisplayNode {
//        std::vector<DisplayNodeController*>;
//        mane::MeshRenderer _renderer;
//
//    public:
//        DisplayNode(mane::Engine* engine);
//        ~DisplayNode();
//    };


}

#endif //METALANCER_DISPLAYLIST_HPP
