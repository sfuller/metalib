//
// Created by Sam Fuller on 6/27/15.
//

#ifndef METALANCER_FONT_HPP
#define METALANCER_FONT_HPP

#include <mane/Texture.hpp>
#include <stuffbuilder/FontProperties.hpp>
#include <memory>

namespace metalancer {

    class Font {

    public:
        stuffbuilder::FontProperties properites;
        std::weak_ptr<mane::Texture> texture;

    };
}

#endif //METALANCER_FONT_H
