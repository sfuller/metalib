//
// Created by sfuller on 8/22/15.
//

#ifndef METALANCER_SIMPLERENDERERVIEW_HPP
#define METALANCER_SIMPLERENDERERVIEW_HPP


#include <metalib/DisplayList.hpp>

namespace metalancer {
    class SimpleRendererView {

    protected:
        DisplayNode node;

    public:
        SimpleRendererView(DisplayList& dl);

        void set_tint(const mane::Color& color);
        virtual void set_transform(const mane::Matrix4& matrix);
        void set_active(bool active);
    };
}


#endif //METALANCER_SIMPLERENDERERVIEW_HPP
