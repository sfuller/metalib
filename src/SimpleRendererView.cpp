//
// Created by sfuller on 8/22/15.
//

#include <metalib/SimpleRendererView.hpp>

using namespace metalancer;
using namespace mane;

SimpleRendererView::SimpleRendererView(DisplayList& dl) :
    node(dl)
{
}

void SimpleRendererView::set_tint(const mane::Color &color) {
    node.renderer().set_uniform("tint", color);
}

void SimpleRendererView::set_transform(const mane::Matrix4 &matrix) {
    node.renderer().set_view(matrix);
}

void SimpleRendererView::set_active(bool active) {
    node.set_active(active);
}