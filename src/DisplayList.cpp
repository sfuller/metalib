//
// Created by sfuller on 8/9/15.
//

#include <metalib/DisplayList.hpp>

using namespace metalancer;
using namespace mane;

DisplayNodeController::DisplayNodeController() :
    renderer(nullptr),
    active(true)
{ }
DisplayNodeController::~DisplayNodeController() { }

LinkedDisplayListController::LinkedDisplayListController(LinkedDisplayListController* prev) :
    prev(prev),
    next(nullptr)
{ }

LinkedDisplayListController::~LinkedDisplayListController() {
    if(prev != nullptr) {
        prev->next = next;
    }
    if(next != nullptr) {
        next->prev = prev;
    }
}

DisplayList::DisplayList(Engine* engine, const Camera* camera) :
    _engine(engine),
    _camera(camera),
    _start(nullptr),
    _end(&_start)
{
    _start.next = &_end;
}

DisplayNodeController* DisplayList::add() {
    LinkedDisplayListController* controller = new LinkedDisplayListController(_end.prev);
    _end.prev->next = controller;
    _end.prev = controller;
    controller->next = &_end;
    return controller;
}

void DisplayList::render() const {
    LinkedDisplayListController* controller = _start.next;
    for(; controller != nullptr; controller = controller->next) {
        MeshRenderer* renderer = controller->renderer;
        if(renderer != nullptr && controller->active) {
            _engine->render(*_camera, *renderer);
        }
    }
}

mane::Engine* DisplayList::engine() const { return _engine; };
const mane::Camera* DisplayList::camera() const { return _camera; }

DisplayNode::DisplayNode(mane::Engine *engine) :
    _controller(nullptr),
    _renderer(engine)
{
}

DisplayNode::DisplayNode(DisplayList& dl) :
    _controller(dl.add()),
    _renderer(dl.engine())
{
    _controller->renderer = &_renderer;
}

DisplayNode::DisplayNode(DisplayNode&& other) :
    _controller(other._controller),
    _renderer(std::move(other._renderer))
{
    other._controller = nullptr;
    _controller->renderer = &_renderer; // New controller pointer since we move constructed.
}

DisplayNode& DisplayNode::operator=(DisplayNode &&other) {
    _renderer = other._renderer; // Should be using move assignment operator
    std::swap(_controller, other._controller);
    return *this;
}

DisplayNode::~DisplayNode() {
    if(_controller != nullptr) {
        delete _controller;
    }
}

MeshRenderer& DisplayNode::renderer() {
    return _renderer;
}

void DisplayNode::set_active(bool active) {
    _controller->active = active;
}