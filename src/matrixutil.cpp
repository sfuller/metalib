//
// Created by Sam Fuller on 10/3/15.
//

#include <metalib/matrixutil.hpp>
#include <cmath>

using namespace mane;

Matrix4 metalancer::make_perspective(float fovy, float aspect, float z_near, float z_far) {
    float f = 1.0f / std::tan(fovy / 2.0f);
    float near_minus_far = z_near - z_far;
    return Matrix4(
        Vector4(f / aspect, 0, 0, 0),
        Vector4(0, f, 0, 0),
        Vector4(0, 0, (z_far + z_near) / near_minus_far, -1),
        Vector4(0, 0, (2 * z_far * z_near) / near_minus_far, 0 )
    );
}


